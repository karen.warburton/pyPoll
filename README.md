# pyPoll
This project called "pyPoll" required some prework before coding could begin:

Inside my local git repository, I needed to create a directory called PyPoll.

Inside of this folder I, added the following:

A new file called main.py. This will be the main script to run for each analysis.
A "Resources" folder that contains the CSV files I used. 
I had to make sure the csv file had the correct path to the CSV file.
An "analysis" folder that contains my text file that has the results from my analysis.

Once the prework was completed as described above, I was required to complete the following steps for this project:

In this challenge, I was tasked with helping a small, rural town modernize its vote counting process.

I was provided with a set of poll data called election_data.csv. The dataset is composed of three columns: Voter ID, County, and Candidate. My task was to create a Python script that analyzes the votes and calculates each of the following:


The total number of votes cast


A complete list of candidates who received votes


The percentage of votes each candidate won


The total number of votes each candidate won


The winner of the election based on popular vote.
Winner: Khan
-------------------------
